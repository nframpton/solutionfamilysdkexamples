﻿using OpenNETCF;
using SolutionFamily;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionFamily.Customer
{
    public class CustomerService : ConfigurableServiceBase<CustomerConfig>, ICustomerService, ILicensedService
    {
        private List<CustomerThing> m_things = new List<CustomerThing>();

        public bool Loaded { get; set; }

        public int StructureCount { get; set; }

        public CustomerService()
            : base(CustomerConfig.ConfigurationSectionName)
        {
        }

        public void OnLicenseChanged(LicensedFeatures newLicense)
        {
            if (newLicense == LicensedFeatures.None)
            {
                Shutdown();
            }
        }

        private void Shutdown()
        {
        }

        protected override void Initialize(CustomerConfig configuration)
        {
            // configuration from the engine.config comes in here
            Loaded = true;
        }

        public override void NoConfigurationInitialize()
        {
            // no config exists - this is a good place to create a default one if you wish
            Loaded = true;
        }

        protected override void AfterAllModulesLoaded()
        {
            base.AfterAllModulesLoaded();

            // we're guaranteed that all Modules have loaded.  
            // This is a good place to load dependent services from other modules
        }

        public void AddStuff(string name, int amount)
        {
            var existing = m_things.FirstOrDefault(t => t.Name == name);
            if (existing != null)
            {
                existing.StuffCount += amount;
            }
        }

        public void AddThing(CustomerThing thing)
        {
            if (thing != null)
            {
                m_things.Add(thing);

                // we could add it to the DB here (needs some attributes, etc)
                // StorageService.GetLocalStore().Insert(thing);
            }
        }

        public CustomerThing[] GetThings()
        {
            return m_things.ToArray();

            // alternatively
            // StorageService.GetLocalStore().Select<FeatureThing>();
        }
    }
}
