﻿using OpenNETCF.IoC;
using OpenNETCF.Web;
using SolutionFamily;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionFamily.Customer
{
    public class CustomerHandler : BaseHttpHandler
    {
        private ICustomerService m_service;

        public override bool IsReusable => true;

        public override string HandlerRoot
        {
            get { return "/api/customer"; }
        }

        private ICustomerService CustomerService
        {
            get
            {
                if (m_service == null)
                {
                    m_service = RootWorkItem.Services.Get<ICustomerService>();
                }
                return m_service;
            }
        }

        protected override void HandleGet(HttpContext context)
        {
            SendJsonResponse(context, CustomerService.GetThings());
        }
    }
}
