﻿using OpenNETCF.IoC;
using SolutionFamily;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionFamily.Customer
{
    public class Module : LicensedModuleBase
    {
        public override void AddServices()
        {
            // turn down demo mode to be just 1 minute (to aid in testing)
            DemoModeLifetimeMinutes = 1;

            // create instances of our own services
            var service = new CustomerService();

            // put it into IoC so the Adapter can get it
            RootWorkItem.Services.Add<ICustomerService>(service);
        }
    }
}
