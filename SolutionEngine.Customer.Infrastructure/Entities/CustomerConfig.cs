﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionFamily.Customer
{
    public class CustomerConfig
    {
        public const string ConfigurationSectionName = "customer";
        public const int DefaultTimeout = 5000;

        public int Timeout { get; set; } = DefaultTimeout;
    }
}
