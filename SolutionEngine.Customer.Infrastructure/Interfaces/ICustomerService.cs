﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionFamily.Customer
{
    public interface ICustomerService
    {
        CustomerThing[] GetThings();
        void AddThing(CustomerThing thing);
        void AddStuff(string name, int amount);
        bool Loaded { get; }
        int StructureCount { get; }
    }
}
