﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenNETCF.IoC;
using SolutionFamily;

namespace SolutionFamily.Customer
{
    public class CustomerDevice : HostedDeviceBase
    {
        private ICustomerService CustomerService;
        private int m_count;
        private CustomerAdapter Adapter;
        private bool m_loaded = false;
        private bool m_configured;
        private string m_status;
        private double m_temperature;
        private Thread m_updateThingsThread;
        private int m_lastStructureCounter = 0;
        public ManualResetEvent m_shutdown = new ManualResetEvent(false);

        public CustomerDevice(CustomerAdapter adapter)
             : base(adapter)
        {
            Adapter = adapter;


            CustomerService = RootWorkItem.Services.Get<ICustomerService>();
            if (CustomerService == null)
            {
                RootWorkItem.Services.Added += new EventHandler<DataEventArgs<object>>(Services_Added);
            }
            else
            {
                LoadThings();
                CreateUpdateThingsThread();
            }
        }
        private void Services_Added(object sender, DataEventArgs<object> e)
        {
            if (e.Data is ICustomerService)
            {
                CustomerService = e.Data as ICustomerService;
                LoadThings();
                CreateUpdateThingsThread();
            }
        }

        private void LoadThingsProc()
        {
            while (CustomerService == null || !CustomerService.Loaded)
            {
                Thread.Sleep(1000);
            }
            LoadThings();

        }

        private void LoadThings()
        {


        }

        private void CreateUpdateThingsThread()
        {
            m_updateThingsThread = new Thread(UpdateThingsProc)
            {
                IsBackground = true,
                Name = "Things Device Updater"
            };
            m_updateThingsThread.Start();
        }

        private void UpdateThingsProc()
        {


        }

        private void Initialize()
        {
            m_loaded = true;
            m_temperature = 42.0;
        }

        public override string Name
        {
            get { return "Customer"; }
        }

        [EventDataItem]
        public bool Configured
        {
            get => m_configured;
            set => SetProperty(ref m_configured, value);
        }

        [EventDataItem]
        public string Status
        {
            get => m_status;
            set => SetProperty(ref m_status, value);
        }

        [EventDataItem]
        public bool Loaded
        {
            get => m_loaded;
            set => SetProperty(ref m_loaded, value);
        }

        [SampleDataItem]
        public double Temperature
        {
            get => m_temperature;
            set => SetProperty(ref m_temperature, value);
        }

        [EngineMethod]
        public void SetConfigured()
        {
            Configured = !Configured;
        }


        [EventDataItem]
        public int ThingCount
        {
            get => m_count;
            set => SetProperty(ref m_count, value);
        }

        [EngineMethod]
        public void AddThing(string name)
        {
            if( CustomerService == null)
            {
                SFTrace.Error("No Customer Service!!!", this.GetType().Name);
            }
            CustomerService.AddThing(new CustomerThing
            {
                Name = name,
                StuffCount = 0
            });
        }

        [EngineMethod]
        public void AddStuff(string name, int amount)
        {
            CustomerService.AddStuff(name, amount);
        }
        public override void OnPublish()
        {
            Temperature += 1.5;
            if (CustomerService != null)
            {
                Loaded = CustomerService.Loaded;
                ThingCount = CustomerService.GetThings().Length;
            }
            base.OnPublish();
        }

    }
}
