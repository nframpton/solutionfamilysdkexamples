﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SolutionFamily;

namespace SolutionFamily.Customer
{
    public class CustomerAdapter : HostedAdapterBase, IDisposable
    {
        // Private Data
        private CustomerDevice m_device;

        public override IHostedDevice HostedDevice
        {
            get { return m_device; }
        }

        public CustomerAdapter()
        {
            m_device = new CustomerDevice(this);
        }
    }
}
